function toggleAdvSearch() {
	$('div.advanced-search').toggle();
}


function toggleRayon() {
	var checkbox = $('#geoloc').is(':checked');
	if (checkbox) {
		$('#label_rayon').css('color','black');
		$('#rayon').prop('disabled', false);
		$('#region').prop('disabled', true);
	}
	else {
		$('#label_rayon').css('color','grey');
		$('#rayon').prop('disabled', true);
		$('#region').prop('disabled', false);
	}
}

$(document).ready( function() {
	$('div.advanced-search').hide();
	toggleRayon();
	$('#more-search').click(toggleAdvSearch);
	$('#geoloc').change(toggleRayon);
});